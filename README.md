## Back-end

El back-end de la aplicación está hecho en .Net Core 2, por lo cual es necesario tener instalado este framework para poder ejecutarlo:

- [.Net Core 2 para GNU/Linux](https://docs.microsoft.com/en-us/dotnet/core/linux-prerequisites?tabs=netcore2x)
- [.Net Core 2 para Windows](https://docs.microsoft.com/en-us/dotnet/core/windows-prerequisites?tabs=netcore2x)
- [.Net Core 2 para macOS](https://docs.microsoft.com/en-us/dotnet/core/macos-prerequisites?tabs=netcore2x)

Una vez instalado el framework, se puede ejecutar la aplicación del back-end mediante los siguientes comandos (asumiendo que la ubicación actual es el directorio del repositorio "Prueba-Xpert"):

1. `cd Back-end`
2. `dotnet build`
3. `dotnet run`

Tener en cuenta que la aplicación back-end se ejecuta sobre el protocolo *HTTPS*.

Para generar la versión de producción de la aplicación back-end se utiliza el comando `dotnet publish`.

---

## Front-end

El front-end de la aplicación esta construido en Angular 7, por lo cual es necesario tener instalado este framework para poder ejecutarlo:

- [Instalación de Angular en cualquier sistema operativo](https://angular.io/guide/quickstart)

Una vez instalado el framework, se debe realizar la instalación de los paquetes necesarios (asumiendo que la ubicación actual es el directorio del repositorio "Prueba-Xpert"):

1. `cd Front-end`
2. `npm i`

Posteriormente se ejecuta la aplicación con el comando:

3. `ng serve -o`

Para generar la versión de producción de la aplicacion front-end se utiliza el comando `ng build --prod`