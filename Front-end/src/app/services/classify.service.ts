import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class ClassifyService {

    basrUrl = "https://localhost:5001/api/";

    constructor(private http: HttpClient) { }

    public ClassifyFile(file: any, criteria) {
        const formData = new FormData();
        formData.append(file.name, file, '/');
        formData.append(criteria, "criteria");
        return this.http.post(this.basrUrl + "classify", formData);
    }

    public DownloadFile(fileName: string) {
        return this.http.get(this.basrUrl + "classify/" + fileName, { responseType: 'blob' });
    }
}
