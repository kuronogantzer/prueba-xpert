import { Component, OnInit } from '@angular/core';
import { ClassifyService } from '../../services/classify.service';
import { saveAs } from 'file-saver';

@Component({
    selector: 'app-upload-files',
    templateUrl: './upload-files.component.html',
    styles: []
})
export class UploadFilesComponent implements OnInit {

    file = undefined;
    fileSuccess = false;
    criteria: string;

    constructor(private classifyService: ClassifyService) { }

    ngOnInit() {
    }

    UpdateFile(event:any) {
        let name = event.target.files[0].name;
        if (/.+\.[D|d][A|a][T|t]/.test(name)) {
            this.file = event.target.files[0];
        } else {
            this.file = undefined;
            this.fileSuccess = false;
            alert("El formato del archivo debe ser .DAT");
        }
    }

    UploadFile() {
        if (this.file) {
            this.classifyService.ClassifyFile(this.file, this.criteria == undefined ? "b" : this.criteria)
            .subscribe( data => {
                this.fileSuccess = true;
                alert("Archivo subido");
            },
            error => {
                this.fileSuccess = false;
                console.log(error);
                alert("Error al cargar archivo");
            });
        }
    }

    DownloadFile(fileName) {
        this.classifyService.DownloadFile(fileName)
            .subscribe( data => {
                saveAs(data, fileName, {
                    type: 'text/plain;charset=utf-8'
                });
            },
            error => {
                console.log(error);
                alert("Error al descargar archivo");
            });
    }
}
