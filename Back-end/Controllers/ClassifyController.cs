﻿using System.Net.Http.Headers;
using System.Net.Mime;
using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Business;
using Microsoft.AspNetCore.Http;

namespace Back_end.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClassifyController : ControllerBase
    {

        private IHostingEnvironment _hostingEnvironment;
        private ClassifyBusiness classify;

        public ClassifyController(IHostingEnvironment hostingEnvironment)
        {
            // Se instancian los objetos para las variables de entorno y la capa de negocio
            this._hostingEnvironment = hostingEnvironment;
            this.classify = new ClassifyBusiness();
        }

        /// <summary>
        /// End point utilizado para descargar enviar archivos desde el back-end hacia el front-end
        /// </summary>
        /// <param name="fileName">Nombre del archivo solicitado por el front-end</param>
        /// <returns>Retorna un objeto con los datos del archivo</returns>
        [HttpGet("{fileName}")]
        public async Task<ActionResult> Get(string fileName)
        {
            try
            {
                string folder = "Cargas";
                // Se genera la ruta donde se almacenan los archivos
                string pathFiles = Path.Combine(this._hostingEnvironment.ContentRootPath, folder);
                // Se realiza la lectura del archivo solicitado y se copian los datos a la memoria
                // para ser enviado en la respuesta.
                MemoryStream memory = new MemoryStream();
                using(FileStream fileS = new FileStream(Path.Combine(pathFiles, fileName), FileMode.Open))
                {
                    await fileS.CopyToAsync(memory);
                }
                memory.Position = 0;
                return File(memory, "txt/plain", fileName);
            }
            catch (System.Exception e)
            {
                return BadRequest("Error al descargar el archivo: \n" + e.Message);
            }
        }

        /// <summary>
        /// End point utilizado para recibir la carga del archivo en formato .DAT
        /// y el criterio para determinar los bovinos dentro del archivo
        /// </summary>
        /// <returns>Retorna True si se logró leer el archivo, almacenarlo y generar los
        /// archivos Bovinos.txt y Equinos.txt, de lo contrario retorna False</returns>
        [HttpPost]
        public async Task<IActionResult> Post()
        {
            try
            {
                IFormFile file = Request.Form.Files[0]; // Se obtiene el archivo de la petición
                string criteria = Request.Form.Keys.FirstOrDefault(); // Se obtiene el criterio
                string folder = "Cargas";
                // Se genera la ruta donde se almacenan los archivos
                string pathFiles = Path.Combine(this._hostingEnvironment.ContentRootPath, folder); 
                
                // Se hace llamado al método para clasificar los bovinos y equinos
                // Si el usuario no ingresa ningún criterio se asume que lo bovinos empiezan por la letra "b"
                bool result = await this.classify.Classify(file, pathFiles, String.IsNullOrEmpty(criteria) ? "b" : criteria);
                return Ok(result);
            }
            catch (System.Exception e)
            {
                return BadRequest("Error al cargar el archivo: \n" + e.Message);
            }
        }
    }
}
