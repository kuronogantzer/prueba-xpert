using System.Text.RegularExpressions;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Business
{
    /// <summary>
    /// Clase para encapsular los métodos de la capa de negocio de la aplicación
    /// </summary>
    public class ClassifyBusiness
    {
        /// <summary>
        /// Método que contiene toda la lógica para leer el archivo cargado por el usuario,
        /// usar el criterio dado para clasificar los bovinos y equinos del archivo
        /// y generar los archivos Bovinos.txt y Equinos.txt
        /// </summary>
        /// <param name="file">Objeto con los datos del archivo cargado por el usuario</param>
        /// <param name="pathFiles">Ruta donde se almacenan los archivos</param>
        /// <param name="criteria">Criterio para clasificar a los bovinos del archivo</param>
        /// <returns>Retorna True si se logró leer el archivo, almacenarlo y generar los
        /// archivos Bovinos.txt y Equinos.txt, de lo contrario retorna False</returns>
        public async Task<bool> Classify(IFormFile file, string pathFiles, string criteria)
        {
            try
            {
                // Expresión regular que usa el criterio dado para evaluar en cada línea del archivo
                // si el texto contiene al inicio el criterio dado o no.
                Regex regex = new Regex(@"^[" + criteria.ToLower() + "+]");
                // Se realiza la lectura y almacenamiento del archivo cargado por el usuario
                string fullNameFile = await Utilities.UploadFilesUtility.UploadFiles(file, pathFiles);
                FileStream inputFile = new FileStream(fullNameFile, FileMode.Open);
                using (StreamReader reader = new StreamReader(inputFile))
                {
                    // Se crean los objetos necesario para realizar la escritura de los archivos
                    // Bovinos.txt y Equinos.txt
                    string fullPathB = Path.Combine(pathFiles, "Bovinos.txt");
                    string fullPathC = Path.Combine(pathFiles, "Equinos.txt");
                    FileStream outputFileB = new FileStream(fullPathB, System.IO.File.Exists(fullPathB) ? FileMode.Truncate : FileMode.Create);
                    FileStream outputFileC = new FileStream(fullPathC, System.IO.File.Exists(fullPathC) ? FileMode.Truncate : FileMode.Create);
                    StreamWriter writerB = new StreamWriter(outputFileB);
                    StreamWriter writerC = new StreamWriter(outputFileC);
                    string line;
                    // Se realiza una lectura línea por línea del archivo cargado para realizar
                    // la clasificación según la expresión regular construida con el criterio
                    while((line = reader.ReadLine()) != null)
                    {
                        if(regex.IsMatch(line.ToLower()))
                            writerB.WriteLine(line);
                        else
                            writerC.WriteLine(line);
                    }
                    writerB.Close();
                    writerC.Close();
                }
                return fullNameFile != null;
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }
    }
   
}