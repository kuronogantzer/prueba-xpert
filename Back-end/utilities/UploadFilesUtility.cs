using System.IO;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Utilities
{
    /// <summary>
    /// Clase para encapsular los métodos de utilidad de la aplicación
    /// </summary>
    public class UploadFilesUtility
    {
        /// <summary>
        /// Método de utilidad para guardar cualquiera archivo en la ruta del servidor dada
        /// </summary>
        /// <param name="file">Datos del archivo que se desea guardar</param>
        /// <param name="pathFiles">Ruta donde se desea guardar el archivo</param>
        /// <returns>Retorna la ruta completa del archivo si el proceso es exitoso.</returns>
        public static async Task<string> UploadFiles(IFormFile file, string pathFiles)
        {
            try
            {
                string fullPath = null;
                if (!Directory.Exists(pathFiles))
                {
                    Directory.CreateDirectory(pathFiles);
                }
                if (file.Length > 0)
                {
                    fullPath = Path.Combine(pathFiles, file.Name);
                    using (var stream = new FileStream(fullPath, System.IO.File.Exists(fullPath) ? FileMode.Truncate : FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                    }
                }
                return fullPath;
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }
    }
    
}